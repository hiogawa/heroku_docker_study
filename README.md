Toy example of deploying dockerized Rails app on Heroku with Heroku Postgres

https://pacific-hollows-68914.herokuapp.com/admin

```
# Setup
heroku login, heroku create, heroku addons:create heroku-postgresql, etc ...

# Deployment
$ docker-compose build builder
$ docker push registry.heroku.com/pacific-hollows-68914/web
$ heroku container:release web

# Set environment variable (config var) e.g.
$ heroku config:set RAILS_ENV=production RAILS_LOG_TO_STDOUT=yes \
                    RAILS_SERVE_STATIC_FILES=yes RAILS_MASTER_KEY=$(cat config/master.key)

# Database migration
$ DATABASE_URL=$(heroku config | grep DATABASE_URL | cut -d ' ' -f 2) docker-compose run web rails db:migrate

# Imitating production setup locally
$ DATABASE_URL='postgres://someone:somepasswd@db:5432/somedb' docker-compose up web db
# and from different terminal
$ DATABASE_URL='postgres://someone:somepasswd@db:5432/somedb' docker-compose run web rails db:migrate
```

References

- https://devcenter.heroku.com/articles/container-registry-and-runtime
- https://devcenter.heroku.com/articles/heroku-postgresql
- https://hub.docker.com/_/ruby
