FROM ruby:2.4.1

# For rails runtime
RUN apt-get update && apt-get install -y nodejs

# version >= 2 is required
ENV BUNDLER_VERSION='2.0.2'
RUN gem install bundler --no-document -v '2.0.2'

# Save time by letting this layer cache compiled native extension
# cf. bundle show --paths | ruby -ne 'puts $_.chomp.split("/").last.gsub(/(\-)(\d+)((\.\d+)*)/, ":\\2\\3 \\") if File.directory?("#{$_.chomp}/ext")'
RUN gem install \
bindex:0.8.1 \
bootsnap:1.4.4 \
byebug:11.0.1 \
childprocess:1.0.1 \
concurrent-ruby:1.1.5 \
ffi:1.11.1 \
msgpack:1.3.1 \
nio4r:2.4.0 \
nokogiri:1.10.3 \
puma:3.12.1 \
rb-fsevent:0.10.3 \
sqlite3:1.4.1 \
thread_safe:0.3.6 \
websocket-driver:0.7.1 \
pg:1.1.4

COPY ./ /app
WORKDIR /app

RUN bundle install --local --without development,test
RUN rails assets:precompile

CMD bundle exec rails s -b 0.0.0.0
