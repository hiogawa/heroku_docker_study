Rails.application.routes.draw do
  namespace :admin do
    resources :comments
    resources :posts
    resources :users

    root to: "users#index"
  end
end
